package br.com.thimilo.longshort.workercotacaoativos.infra.entrypoint.kafka;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import br.com.thimilo.longshort.workercotacaoativos.core.usecase.ProcessarEventoRetryCadastroUseCase;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

@Component
@KafkaListener(topics = "${app.kafka.retry.consumer.cadastroativos.topic-name}")
public class ConsumerRetryKafkaListener {

	private final ProcessarEventoRetryCadastroUseCase processarEventoRetryCadastroUseCase;
	private static final long TEMPO_RETRY_MILISEGUNDOS = 5000;

	public ConsumerRetryKafkaListener(ProcessarEventoRetryCadastroUseCase processarEventoRetryCadastroUseCase) {
		this.processarEventoRetryCadastroUseCase = processarEventoRetryCadastroUseCase;
	}

	@KafkaHandler
	public void receberEventoCadastroAtivo(AtivoRequest request,Acknowledgment acknowledgment,
			@Header(KafkaHeaders.RECEIVED_TIMESTAMP)long timesTamp) throws InterruptedException {
	
		
		long segundos = LocalDateTime.now().minus(timesTamp,ChronoUnit.MILLIS).getSecond() *1000l;
		if(segundos >= TEMPO_RETRY_MILISEGUNDOS) {
			processarEventoRetryCadastroUseCase.executar(request);
			acknowledgment.acknowledge();
		}else {
			acknowledgment.nack(TEMPO_RETRY_MILISEGUNDOS - segundos);
		}
	}
}

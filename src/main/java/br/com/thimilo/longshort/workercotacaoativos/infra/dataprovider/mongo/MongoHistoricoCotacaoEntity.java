package br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.mongo;

import java.time.LocalDate;
import java.time.LocalTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "historicoCotacao")
public class MongoHistoricoCotacaoEntity {
	
	@Id
	@Field("_id")
	private String id;
	
	@Field
	private String codigoAtivo;
	
	@Field
	private Double valorCotacao;
	
	@Field
	private LocalDate data;
	
	@Field
	private LocalTime hora;
	
	

	public MongoHistoricoCotacaoEntity() {
		super();
	}


	public MongoHistoricoCotacaoEntity(String id, String codigoAtivo, Double valorCotacao, LocalDate data) {
		super();
		this.id = id;
		this.codigoAtivo = codigoAtivo;
		this.valorCotacao = valorCotacao;
		this.data = data;
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCodigoAtivo() {
		return codigoAtivo;
	}

	public void setCodigoAtivo(String codigoAtivo) {
		this.codigoAtivo = codigoAtivo;
	}

	public Double getValorCotacao() {
		return valorCotacao;
	}

	public void setValorCotacao(Double valorCotacao) {
		this.valorCotacao = valorCotacao;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}
	
	

}

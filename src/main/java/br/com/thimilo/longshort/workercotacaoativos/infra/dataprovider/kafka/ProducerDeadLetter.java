package br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemDeadLetterGateway;

@Component
@Profile("prod")
public class ProducerDeadLetter implements GerarMensagemDeadLetterGateway {

	@Value(value = "${app.kafka.deadletter.consumer.cadastroativos.topic-name}")
	private String kafkaTopicName;

	private KafkaTemplate<String, AtivoDeadLetterResponse> kafkaTemplate;

	public ProducerDeadLetter(KafkaTemplate<String, AtivoDeadLetterResponse> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerar(AtivoDeadLetterResponse response) {
		kafkaTemplate.send(kafkaTopicName, response);
	}

}

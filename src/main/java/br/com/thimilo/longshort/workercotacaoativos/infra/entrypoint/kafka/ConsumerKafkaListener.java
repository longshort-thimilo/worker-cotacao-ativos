package br.com.thimilo.longshort.workercotacaoativos.infra.entrypoint.kafka;

import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Component;

import br.com.thimilo.longshort.workercotacaoativos.core.usecase.ProcessarEventoCadastroUseCase;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;

@Component
@KafkaListener(topics = "${app.kafka.consumer.cadastroativos.topic-name}")
public class ConsumerKafkaListener {

	private final ProcessarEventoCadastroUseCase processarEventoCadastroUseCase;

	public ConsumerKafkaListener(ProcessarEventoCadastroUseCase processarEventoCadastroUseCase) {
		this.processarEventoCadastroUseCase = processarEventoCadastroUseCase;
	}

	@KafkaHandler
	public void receberEventoCadastroAtivo(AtivoRequest request, Acknowledgment acknowledgment) {
		acknowledgment.acknowledge();
		processarEventoCadastroUseCase.executar(request);
	}

}

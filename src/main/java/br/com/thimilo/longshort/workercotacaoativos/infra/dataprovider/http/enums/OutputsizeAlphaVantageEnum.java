package br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.http.enums;

public enum OutputsizeAlphaVantageEnum {
	COMPACT, FULL;
}

package br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.thimilo.longshort.workercotacaoativos.core.entity.HistoricoCotacao;


public interface BuscarCotacoesSalvasPorAtivoGateway {

	HistoricoCotacao buscarPrimeiraCotacao(String codigoAtivo);
}

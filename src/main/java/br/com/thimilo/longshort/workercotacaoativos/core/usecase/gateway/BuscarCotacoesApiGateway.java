package br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway;

import java.time.LocalDate;

import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;

public interface BuscarCotacoesApiGateway {

	HistoricoCotacaoResponse buscar(String codigoAtivo, LocalDate inicio,
			LocalDate fim,boolean full);
}

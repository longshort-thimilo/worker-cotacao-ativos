package br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemRetryGateway;

@Component
@Profile("prod")
public class ProducerRetry implements GerarMensagemRetryGateway {
	
	@Value(value = "${app.kafka.retry.consumer.cadastroativos.topic-name}")
	private String kafkaTopicName;
	
	private KafkaTemplate<String,AtivoRequest> kafkaTemplate;

	public ProducerRetry(KafkaTemplate<String,AtivoRequest> kafkaTemplate) {
		this.kafkaTemplate = kafkaTemplate;
	}

	@Override
	public void gerar(AtivoRequest request) {
		kafkaTemplate.send(kafkaTopicName, request);
	}

}

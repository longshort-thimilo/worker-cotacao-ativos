package br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway;

public interface ConfiguracaoGateway {
	
	Integer getQuantidadeDiasParaCotacaoNoCadastroAtivo();
}

package br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.http.enums;

public enum FuncaoAlphaVantageEnum {
	TIME_SERIES_INTRADAY, TIME_SERIES_DAILY;
}

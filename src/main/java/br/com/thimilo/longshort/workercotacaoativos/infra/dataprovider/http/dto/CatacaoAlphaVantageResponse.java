package br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.http.dto;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CatacaoAlphaVantageResponse {

	@JsonProperty(value = "Meta Data")
	private CotacaoAlphaVantageMetaData  cotacaoAlphaVantageMetaData;
	
	@JsonProperty(value = "Time Series (Daily)")
	private Map<String,CotacaoAlphaVantageData>  timeseries;
	
	public Map<String, CotacaoAlphaVantageData> getTimeseries() {
		return timeseries;
	}

	public void setTimeseries(Map<String, CotacaoAlphaVantageData> timeseries) {
		this.timeseries = timeseries;
	}

	public CotacaoAlphaVantageMetaData getCotacaoAlphaVantageMetaData() {
		return cotacaoAlphaVantageMetaData;
	}
	
	public void setCotacaoAlphaVantageMetaData(CotacaoAlphaVantageMetaData cotacaoAlphaVantageMetaData) {
		this.cotacaoAlphaVantageMetaData = cotacaoAlphaVantageMetaData;
	}

	


}

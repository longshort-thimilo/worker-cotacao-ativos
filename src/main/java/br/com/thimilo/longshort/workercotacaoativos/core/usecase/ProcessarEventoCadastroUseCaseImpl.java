package br.com.thimilo.longshort.workercotacaoativos.core.usecase;

import java.time.LocalDate;
import java.util.Objects;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import br.com.thimilo.longshort.base.dto.response.ListaErroEnum;
import br.com.thimilo.longshort.base.dto.response.ResponseDataErro;
import br.com.thimilo.longshort.base.gateway.SalvarGateway;
import br.com.thimilo.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.AtivoRequest;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.HistoricoCotacaoResponse;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesApiGateway;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorAtivoGateway;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway.ConfiguracaoGateway;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemDeadLetterGateway;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway.GerarMensagemRetryGateway;

@Component
public class ProcessarEventoCadastroUseCaseImpl implements ProcessarEventoCadastroUseCase {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());

	private final SalvarGateway<HistoricoCotacao> salvarGateway;
	private final BuscarCotacoesApiGateway buscarCotacoesGateway;
	private final GerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway;
	private final ConfiguracaoGateway configuracaoGateway;
	private final GerarMensagemRetryGateway gerarMensagemRetryGateway;
	private final BuscarCotacoesSalvasPorAtivoGateway buscarCotacoesSalvasPorAtivoGateway;

	public ProcessarEventoCadastroUseCaseImpl(SalvarGateway<HistoricoCotacao> salvarGateway,
			BuscarCotacoesApiGateway buscarCotacoesGateway,
			GerarMensagemDeadLetterGateway gerarMensagemDeadLetterGateway, ConfiguracaoGateway configuracaoGateway,
			GerarMensagemRetryGateway gerarMensagemRetryGateway,
			BuscarCotacoesSalvasPorAtivoGateway buscarCotacoesSalvasPorAtivoGateway) {
		this.salvarGateway = salvarGateway;
		this.buscarCotacoesGateway = buscarCotacoesGateway;
		this.gerarMensagemDeadLetterGateway = gerarMensagemDeadLetterGateway;
		this.configuracaoGateway = configuracaoGateway;
		this.gerarMensagemRetryGateway = gerarMensagemRetryGateway;
		this.buscarCotacoesSalvasPorAtivoGateway = buscarCotacoesSalvasPorAtivoGateway;
	}

	@Override
	public Void executar(AtivoRequest request) {
		logger.info("Processando evento para carregar histórico de cotações após cadastro de ativo. Código ativo {}",
				request.getCodigo());
		AtivoDeadLetterResponse response = new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao());
		validarCamposObrigatorios(request, response);
		if (!response.getResponse().getErros().isEmpty()) {
			logger.warn("Gerando mensagem de deadletter");
			gerarMensagemDeadLetterGateway.gerar(response);
			return null;
		}

		if (Objects.nonNull(buscarCotacoesSalvasPorAtivoGateway.buscarPrimeiraCotacao(request.getCodigo()))) {
			logger.warn("Ignorando mensagem duplicada para ativo {}", request.getCodigo());
			return null;
		}
		LocalDate dataFinal = LocalDate.now();
		LocalDate dataInicial = dataFinal.minusDays(configuracaoGateway.getQuantidadeDiasParaCotacaoNoCadastroAtivo());
		logger.info("Iniciando busca das cotações no periodo de {} até {}", dataInicial, dataFinal);
		HistoricoCotacaoResponse historicoCotacaoResponse = buscarCotacoes(request, request.getCodigo(), dataInicial,
				dataFinal, 0);
		if (historicoCotacaoResponse.getResponse().getErros().isEmpty()) {
			historicoCotacaoResponse.getHistoricos().forEach(salvarGateway::salvar);
		}
		return null;
	}

	private HistoricoCotacaoResponse buscarCotacoes(AtivoRequest request, String codigo, LocalDate inicio,
			LocalDate fim, int tentativas) {
		HistoricoCotacaoResponse historicoCotacaoResponse = buscarCotacoesGateway.buscar(codigo, inicio, fim, true);
		if (!historicoCotacaoResponse.getResponse().getErros().isEmpty()) {
			if (historicoCotacaoResponse.getResponse().getErros().stream()
					.filter(e -> ListaErroEnum.TIMEOUT.equals(e.getTipo())).count() > 0) {
				if (tentativas < 2) {
					return buscarCotacoes(request, codigo, inicio, fim, ++tentativas);
				} else {
					logger.info("Gerando mensagem de retry do ativo {}", request.getCodigo());
					gerarMensagemRetryGateway.gerar(request);
				}
			} else {
				logger.info("Gerando mensagem de deadLetter do ativo {}", request.getCodigo());
				gerarMensagemDeadLetterGateway
						.gerar(new AtivoDeadLetterResponse(request.getCodigo(), request.getDescricao()));
			}
		}
		return historicoCotacaoResponse;

	}

	private void validarCamposObrigatorios(AtivoRequest request, AtivoDeadLetterResponse response) {
		if (StringUtils.isBlank(request.getCodigo())) {
			String msg = "O campo código é obrigatório";
			logger.error(msg);
			response.getResponse().adcionarErros(new ResponseDataErro(msg, ListaErroEnum.CAMPOS_OBRIGATORIOS));
		}

	}

}

package br.com.thimilo.longshort.workercotacaoativos.core.entity;

import java.time.LocalDate;
import java.time.LocalTime;

import br.com.thimilo.longshort.base.entity.BaseEntity;

public class HistoricoCotacao extends BaseEntity {

	private String codigoAtivo;
	private Double valorCotacao;
	private LocalDate data;
	private LocalTime hora;
	
	public HistoricoCotacao() {

	}
	
	public HistoricoCotacao(String codigoAtivo, Double valorCotacao, LocalDate data) {
		this.codigoAtivo = codigoAtivo;
		this.valorCotacao = valorCotacao;
		this.data = data;
	}
	
	public HistoricoCotacao(String id, String codigoAtivo, Double valorCotacao, LocalDate data) {
		this.id = id;
		this.codigoAtivo = codigoAtivo;
		this.valorCotacao = valorCotacao;
		this.data = data;
	}

	public String getCodigoAtivo() {
		return codigoAtivo;
	}
	public void setCodigoAtivo(String codigoAtivo) {
		this.codigoAtivo = codigoAtivo;
	}
	public Double getValorCotacao() {
		return valorCotacao;
	}
	public void setValorCotacao(Double valorCotacao) {
		this.valorCotacao = valorCotacao;
	}
	public LocalDate getData() {
		return data;
	}
	public void setData(LocalDate data) {
		this.data = data;
	}
	public LocalTime getHora() {
		return hora;
	}
	public void setHora(LocalTime hora) {
		this.hora = hora;
	}
	
	
	
	@Override
	public String toString() {
		return "HistoricoCotacao [codigoAtivo=" + codigoAtivo + ", valor=" + valorCotacao + ", data=" + data + ", hora=" + hora
				+ "]";
	}

	
	

}

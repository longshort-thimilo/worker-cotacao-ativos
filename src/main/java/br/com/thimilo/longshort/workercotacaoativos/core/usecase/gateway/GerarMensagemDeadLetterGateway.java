package br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway;

import br.com.thimilo.longshort.workercotacaoativos.core.usecase.dto.AtivoDeadLetterResponse;

public interface GerarMensagemDeadLetterGateway {

	void gerar(AtivoDeadLetterResponse response);
}

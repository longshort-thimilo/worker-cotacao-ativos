package br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.mongo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import br.com.thimilo.longshort.base.gateway.SalvarGateway;
import br.com.thimilo.longshort.workercotacaoativos.core.entity.HistoricoCotacao;
import br.com.thimilo.longshort.workercotacaoativos.core.usecase.gateway.BuscarCotacoesSalvasPorAtivoGateway;

@Component
public class MongoDataProvider implements BuscarCotacoesSalvasPorAtivoGateway, SalvarGateway<HistoricoCotacao> {
	
	private final HistoricoCotacaoMongoRepository mongoRepository;

	public MongoDataProvider(HistoricoCotacaoMongoRepository mongoRepository) {
		this.mongoRepository = mongoRepository;
	}

	@Override
	public HistoricoCotacao salvar(HistoricoCotacao hist) {
		
		mongoRepository.save(
			new MongoHistoricoCotacaoEntity(hist.getId(), hist.getCodigoAtivo(), hist.getValorCotacao(),hist.getData()));
		return hist;
	}

	@Override
	public HistoricoCotacao buscarPrimeiraCotacao(String codigoAtivo) {
		 Page<MongoHistoricoCotacaoEntity> pageHist = mongoRepository.findByCodigoAtivo(codigoAtivo, PageRequest.of(0, 1));
		 if(pageHist.getNumberOfElements() > 0) {
			 MongoHistoricoCotacaoEntity mongoEntity = pageHist.getContent().get(0);
			 return new HistoricoCotacao(mongoEntity.getId(),mongoEntity.getCodigoAtivo(), mongoEntity.getValorCotacao(), mongoEntity.getData());
		 }
		 else {
			 return null;
		 }
	}

}

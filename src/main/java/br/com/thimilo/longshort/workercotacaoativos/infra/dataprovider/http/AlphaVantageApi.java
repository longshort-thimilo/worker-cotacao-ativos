package br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.http;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.thimilo.longshort.workercotacaoativos.infra.dataprovider.http.dto.CatacaoAlphaVantageResponse;

@FeignClient(url = "https://www.alphavantage.co/", name="alphaVantageApi")
public interface AlphaVantageApi {
	
	@GetMapping(value ="query?function={funcao}&symbol={simbolo}&apikey={apiKey}", produces = MediaType.APPLICATION_JSON_VALUE)
	CatacaoAlphaVantageResponse buscarCotacoes(@RequestParam("funcao") String funcao, 
			@RequestParam("simbolo") String simbolo, @RequestParam("apiKey") String apiKey, @RequestParam("outputsize") String outputsize);

}
